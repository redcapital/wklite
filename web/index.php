<?php
define('ROOT', __DIR__ . '/..');

require ROOT . '/vendor/autoload.php';

use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

$app = new Silex\Application;
$app['debug'] = getenv('APPLICATION_ENV') === 'development';

$app->register(new Wklite\PdoServiceProvider);
$app->register(
    new Silex\Provider\SessionServiceProvider,
    array(
        'session.storage.options' => array(
            'cookie_lifetime' => 3600 * 24 * 14,
        ),
    )
);
$app['session.db_options'] = array(
    'db_table' => 'session',
    'db_id_col' => 'session_id',
    'db_data_col' => 'session_value',
    'db_time_col' => 'session_time',
);
$app['session.storage.handler'] = $app->share(function() use ($app) {
    return new PdoSessionHandler(
        $app['pdo'],
        $app['session.db_options'],
        $app['session.storage.options']
    );
});

$app->register(new Wklite\WkClientProvider);
$app->register(new Silex\Provider\TwigServiceProvider, array(
    'twig.path' => ROOT . '/views',
));
$app['twig'] = $app->share($app->extend('twig', function($twig, $app) {
    $url = function($path) use ($app) {
        return $app['request']->getBaseUrl() . $path;
    };
    $twig->addFunction(new Twig_SimpleFunction('url', $url, array(
        'is_safe' => array('html'),
    )));

    return $twig;
}));

$app->before(function(Request $request) use ($app) {
    $session = $request->getSession();
    $session->start();
    if ($request->attributes->get('_route') === 'login') {
        return;
    }
    if (!$session->has('username')) {
        return $app->redirect('/login');
    }
});

$app->get('/', function(Request $request) use ($app) {
    return $app['twig']->render('home.twig', array(
        'reviewCount' => $app['wkclient']->reviewCount(),
    ));
});

$app->match('/login', function(Request $request) use ($app) {
    $vars = array('username' => '', 'password' => '');
    if ($request->getMethod() === 'POST') {
        $vars['username'] = $request->request->get('username', '');
        $vars['password'] = $request->request->get('password', '');
        $result = $app['wkclient']->signin($vars['username'], $vars['password']);
        if ($result) {
            $request->getSession()->set('username', $vars['username']);
            return $app->redirect('/');
        }
        $vars['error'] = 'Invalid username or password';
    }
    return $app['twig']->render('login.twig', $vars);
})->method('GET|POST')->bind('login');

$app->get('/logout', function(Request $request) use ($app) {
    $request->getSession()->invalidate();
    return $app->redirect('/');
});

$app->get('/review', function(Request $request) use ($app) {
    $app['wkclient']->startReview();
    return $app['twig']->render('review.twig');
});

$app->get('/question', function(Request $request) use ($app) {
    return $app->json($app['wkclient']->getQuestion());
});

$app->post('/answer', function(Request $request) use ($app) {
    $result = $app['wkclient']->postAnswer(
        $request->request->get('itemname', ''),
        $request->request->get('answer', ''),
        $request->request->get('know', 0)
    );
    return $app->json($result);
});

$app->error(function(\Exception $e, $code) use ($app) {
    $isAjax = $app['request']->isXmlHttpRequest();
    if (!$isAjax && $app['debug']) {
        return;
    }
    if ($e instanceof \Guzzle\Common\Exception\GuzzleException) {
        $errorType = 'Guzzle error';
    } elseif ($e instanceof \Wklite\WkClientException) {
        $errorType = 'WkClient error';
    } else {
        $errorType = 'General error';
    }
    if ($isAjax) {
        return new Response(
            "$errorType: " . $e->getMessage()
        );
    } else {
        return $app['twig']->render('error.twig', array(
            'errorType' => $errorType,
            'exception' => $e,
            'code' => $code,
        ));
    }
});

$app->run();
