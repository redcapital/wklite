Zepto(function($) {

  // AJAX facilities
  // ===============

  var ajaxLoading = false, lastQuestion;

  $(document)
    .on('ajaxBeforeSend', function() {
      ajaxLoading = true;
      $loadingIndicator.show();
    })
    .on('ajaxError', function(xhr, options, error) {
      var title = 'Error: ' + options.status + ' ' + options.statusText;
      var error = options.response;
      if (error.length > 200) {
        error = error.substr(0, 200) + '...';
      }
      alert(title + "\n\n" + error);
    })
    .on('ajaxComplete', function() {
      ajaxLoading = false;
      $loadingIndicator.hide();
    });

  $.ajaxSettings.dataType = 'json';
  $.ajaxSettings.cache = false;
  $.ajaxSettings.timeout = 30000;

  function getQuestion(callback) {
    $.get(wklite.baseUrl + '/question', function(data) {
      lastQuestion = data;
      window.setTimeout(function() {
        callback(data);
      }, 100);
    });
  }

  function postAnswer(know, callback) {
    var params = {
      'itemname': lastQuestion.itemname, 
      'answer': lastQuestion.answer,
      'know': know
    };
    $.post(wklite.baseUrl + '/answer', params, function(results) {
      window.setTimeout(function() {
        callback(results);
      }, 100);
    }, 'json');
  }

  // UI facilities
  // =============

  var 
    $reviewArea = $('#reviewArea'),
    $loadingIndicator = $('#loadingIndicator'),
    $progressCompleted = $('#progressCompleted'),
    $progressRest = $('#progressRest'),
    $character = $('#character'),
    $question = $('#question'),
    $questionItem = $('#question > span'),
    $questionType = $('#question > strong'),
    $show = $('#show'),
    $answer = $('#answer'),
    $answerButtons = $('#answerButtons'),
    $showDetails = $('#showDetails'),
    $answerDetails = $('#answerDetails');

  function reset() {
    $reviewArea.css('display', 'none');
  }

  function showNextQuestion() {
    if (ajaxLoading) return;
    getQuestion(function(question) {
      $character.attr('class', 'character ' + question.item).html(question.question).show();
      $questionItem.html(question.item.charAt(0).toUpperCase() + question.item.slice(1));
      $questionType.html(question.asking);
      $question.show();
      $show.show();
      $answer.hide().html(question.answer);
      $answerButtons.hide();
      $showDetails.hide();
      $answerDetails.hide().html(question.details);
    });
  }

  function resultsArrived(results) {
    $progressCompleted.html(results.completed);
    $progressRest.html(' / ' + results.available + ', ' + results.percentage + '%');
    if (results.available === 0) {
      $reviewArea.children().hide();
      alert('You have finished all reviews');
    } else {
      showNextQuestion();
    }
  }

  $reviewArea.children().hide();
  $reviewArea.show();
  $('#status').show();

  $('#reloadQuestion').on('click', function() {
    showNextQuestion();
    return false;
  }).triggerHandler('click');

  $show.on('click', function() {
    $show.hide();
    $answer.show();
    $answerButtons.show();
    $showDetails.show();
  });

  $showDetails.on('click', function() {
    $showDetails.hide();
    $answerDetails.show();
  });

  $('#answerNo').on('click', function() {
    if (ajaxLoading) return;
    $answerButtons.hide();
    postAnswer(0, resultsArrived);
  });

  $('#answerYes').on('click', function() {
    if (ajaxLoading) return;
    $answerButtons.hide();
    postAnswer(1, resultsArrived);
  });
});
