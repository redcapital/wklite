<?php
namespace Wklite;

use Silex\Application,
    Silex\ServiceProviderInterface,
    Symfony\Component\HttpKernel\KernelEvents;

class WkClientProvider implements ServiceProviderInterface
{
    private $app;

    public function register(Application $app)
    {
        $this->app = $app;
        $app['wkclient'] = $app->share(function() use ($app) {
            return new WkClient($app['session']);
        });
    }

    public function onKernelResponse()
    {
        $this->app['wkclient']->persistCookies();
    }

    public function boot(Application $app)
    {
        $app['dispatcher']->addListener(
            KernelEvents::RESPONSE,
            array($this, 'onKernelResponse')
        );
    }
}

