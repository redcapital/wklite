<?php
namespace Wklite;

use Silex\Application,
    Silex\ServiceProviderInterface;

class PdoServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $config = parse_url(getenv('DATABASE_URL'));
        $config['path'] = trim($config['path'], '/');
        if ($config['scheme'] == 'postgres') {
            $dsn = sprintf(
                'pgsql:host=%s;port=%d;dbname=%s',
                $config['host'],
                $config['port'],
                $config['path']
            );
        } elseif ($config['scheme'] == 'mysql') {
            $dsn = sprintf(
                'mysql:host=%s;port=%d;dbname=%s',
                $config['host'],
                $config['port'],
                $config['path']
            );
        }
        $username = isset($config['user']) ? $config['user'] : null;
        $password = isset($config['pass']) ? $config['pass'] : null;
        $app['pdo'] = $app->share(function() use ($dsn, $username, $password) {
            return new \Pdo($dsn, $username, $password);
        });
    }

    public function boot(Application $app)
    {
    }
}

