<?php
namespace Wklite;

use Guzzle\Plugin\Cookie\CookieJar\ArrayCookieJar as BaseArrayCookieJar,
    Guzzle\Plugin\Cookie\Cookie;

/**
 * Tweaks ArrayCookieJar from Guzzle package to work around session cookie problem.
 */
class ArrayCookieJar extends BaseArrayCookieJar
{
    /**
     * Returns all cookies applicable for given parameters.
     * This is exactly the same as original ArrayCookieJar, except it does NOT
     * discard cookies which do not have expiration and maxage (i.e. session
     * cookies), so that they're persisted and sent properly.
     */
    public function all($domain = null, $path = null, $name = null, $skipDiscardable = false, $skipExpired = true)
    {
        return array_values(array_filter($this->cookies, function (Cookie $cookie) use (
            $domain,
            $path,
            $name,
            $skipDiscardable,
            $skipExpired
        ) {
            return false === (($name && $cookie->getName() != $name) ||
                ($skipExpired && $cookie->isExpired()) ||
                ($skipDiscardable && $cookie->getDiscard()) ||
                ($path && !$cookie->matchesPath($path)) ||
                ($domain && !$cookie->matchesDomain($domain)));
        }));
    }
}
