<?php
namespace Wklite;

use Symfony\Component\HttpFoundation\Session\Session,
    Guzzle\Http\Client,
    Guzzle\Plugin\Cookie\CookiePlugin,
    Symfony\Component\DomCrawler\Crawler,
    Symfony\Component\DomCrawler\Form;

/**
 * Talks to Wanikani website.
 */
class WkClient
{
    protected $session;

    protected $client;

    public $cookieJar;

    const URL = 'http://www.wanikani.com';

    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->client = new Client(static::URL);
        $this->client->setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.63 Safari/537.31');

        $this->cookieJar = new ArrayCookieJar;
        $cookies = $this->session->get('wkclient_cookies');
        if ($cookies) {
            $this->cookieJar->unserialize($cookies);
        }
        $cookiePlugin = new CookiePlugin($this->cookieJar);
        $this->client->addSubscriber($cookiePlugin);
    }

    public function persistCookies()
    {
        $this->session->set('wkclient_cookies', $this->cookieJar->serialize());
    }

    /**
     * Tries to sign in with given credentials. Returns true if signed in
     * successfully and false otherwise.
     */
    public function signin($username, $password)
    {
        $request = $this->client->get('/login');
        $response = $request->send();

        $crawler = new Crawler($response->getBody(true), $this->client->getBaseUrl());
        $form = $crawler->selectButton('Sign In')->form(array(
            'user[remember_me]' => 1,
            'user[login]' => $username,
            'user[password]' => $password,
        ));
        $request = $this->client->post($form->getUri(), null, $form->getValues());
        $response = $request->send();
        if (substr_compare($response->getEffectiveUrl(), 'dashboard', -9) != 0) {
            return false;
        }
        return true;
    }

    public function reviewCount()
    {
        $request = $this->client->get('/review');
        $response = $request->send();
        $this->checkAuthenticationStatus($response);

        preg_match(
            '/Dashboard\.startButton\((\d+)\)/miu',
            $response->getBody(true),
            $matches
        );
        return isset($matches[1]) ? (int)$matches[1] : 0;
    }

    public function startReview()
    {
        $request = $this->client->get('/review/session/start');
        $response = $request->send();
        $this->checkAuthenticationStatus($response);

        $crawler = new Crawler($response->getBody(true), $this->client->getBaseUrl());

        $this->session->set('wkclient_csrfToken', $crawler->filter('meta[name="csrf-param"]')->attr('content'));
        $this->session->set('wkclient_csrfTokenValue', $crawler->filter('meta[name="csrf-token"]')->attr('content'));
    }

    public function getQuestion()
    {
        $request = $this->client->get(
            '/review/session/question',
            array(
                'X-Requested-With' => 'XMLHttpRequest',
                'X-CSRF-Token' => $this->session->get('wkclient_csrfTokenValue'),
            )
        );
        $response = $request->send();
        $contents = $response->getBody(true);
        $contents = trim($contents);
        if (empty($contents)) {
            return array('finished' => true);
        }

        $question = $this->parseJson($contents);

        return $this->buildQuestion($question);
    }

    public function postAnswer($itemName, $answer, $know)
    {
        $know = (int)$know;
        if ($know === 0) {
            // Guaranteed to be incorrect
            $answer = 'asdfasfd';
        } else {
            list($answer) = preg_split('/,/u', $answer, 2, PREG_SPLIT_NO_EMPTY);
        }
        $csrfValue = $this->session->get('wkclient_csrfTokenValue');
        $fields = array(
            'utf8' => '✓',
            '_method' => 'put',
            $this->session->get('wkclient_csrfToken') => $csrfValue,
            'item_name' => $itemName,
            'user_response' => $answer,
        );
        $request = $this->client->post(
            '/review/session/answer',
            array(
                'X-Requested-With' => 'XMLHttpRequest',
                'X-CSRF-Token' => $csrfValue,
                'Accept' => '*/*;q=0.5, text/javascript, application/javascript, application/ecmascript, application/x-ecmascript',
                'Accept-Charset' => 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
                'Accept-Language' => 'en-US,en;q=0.8,ru;q=0.6',
            ),
            $fields
        );
        $response = $request->send();

        $result = $this->parseJson($response->getBody(true));
        if (!isset($result['result'], $result['result']['correct'])) {
            throw new WkClientException('Got malformed response from Wanikani');
        }
        if ($know && !$result['result']['correct']) {
            throw new WkClientException('Wanikani says answer is incorrect');
        }
        return array(
            'available' => (int)$result['result']['available_count'],
            'completed' => (int)$result['result']['completed_count'],
            'percentage' => (float)$result['result']['percentage'],
        );
    }

    protected function buildQuestion($question)
    {
        $result = array();
        list($item) = explode(':', $question['item']);
        $result['itemname'] = $question['item'];
        if ($item === 'v') {
            $result['item'] = 'vocabulary';
        } elseif ($item === 'k') {
            $result['item'] = 'kanji';
        } else {
            $result['item'] = 'radical';
        }
        if (isset($question['info']['image']) && !empty($question['info']['image'])) {
            $result['question'] = '<img src="' . $question['info']['image'] . '">';
        } else {
            $result['question'] = $question['info']['character'];
        }
        if ($question['q'] === 'r') {
            $result['asking'] = 'Reading';
            // Reading is available only for kanjis and vocabulary
            if ($item === 'k') {
                $important = $question['info']['important_reading'];
                $result['answer'] = $question['info'][$important];
            } else {
                $result['answer'] = $question['info']['reading'];
            }
        } else {
            $result['asking'] = ($item === 'r') ? 'Name' : 'Meaning';
            if ($item === 'r') {
                $result['asking'] = 'Name';
                $result['answer'] = $question['info']['name'];
            } else {
                $result['asking'] = 'Meaning';
                $result['answer'] = $question['info']['meaning'];
            }
        }

        $result['details'] = 'unknown';

        return $result;
    }

    protected function buildDetails()
    {
    }

    protected function checkAuthenticationStatus($response)
    {
        if (substr_compare($response->getEffectiveUrl(), 'login', -5) === 0) {
            throw new WkClientException("You've been logged out of Wanikani. Sign in again.");
        }
    }

    protected function parseJson($responseBody)
    {
        $opening = strpos($responseBody, '{');
        $closing = strrpos($responseBody, '}');

        if ($opening === false || $closing === false) {
            throw new WkClientException('Cannot parse response json');
        }

        $result = json_decode(
            substr($responseBody, $opening, $closing - $opening + 1),
            true
        );
        if (is_null($result)) {
            throw new WkClientException('Cannot parse response json');
        }
        return $result;
    }
}
