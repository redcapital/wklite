# wklite - lightweight Wanikani

wklite is a ligthweight interface to Wanikani (http://www.wanikani.com)
designed to run on low end smartphones and/or browsers not supported by
Wanikani. Currently I've only tested it in Opera Mobile 12, however it should
run on any browser with HTML and javascript support. It is also optimized for
low bandwidth usage, no more megabytes of unnecessary traffic.

The only thing expected from your phone/browser is the ability to display
Japanese characters.

The interface intentionally shows only "Yes" and "No" answer buttons, since
devices targeted by wklite usually don't support input methods (also I hate
typing anything using phone). Wanikani's input method implemented in javascript
does not work even on Opera Mobile.

You can sign in with your Wanikani account and do reviews, all of your progress
will be properly saved in a Wanikani.

# How it works

wklite is just a proxy between a browser and Wanikani's server. It issues the
same HTTP requests sent by a regular browser and processes responses, so
Wanikani sees it as a regular desktop browser. Why not API? Because API
provides only read-only access and even there there's only limited information
(no review queue).

# Deployment

wklite is written in PHP 5.3 and should run on any decent PHP hosting. At the
moment, it persists sessions into a database, and you're expected to configure
`DATABASE_URL` environment variable, for example in Apache:

    SetEnv DATABASE_URL mysql://user:pass@hostname:port/database

This makes it easy to run wklite on Heroku which already has correct
`DATABASE_URL` set up.

If you don't want to persist sessions to database, edit `web/index.php` and
remove `session.storage.handler` configuration key.

